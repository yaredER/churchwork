# Servicio docker para apps

## @Autor: Arturo Yared Elizondo Regino
## @Version: v1.1

## Ejecutar contenedor :shipit:

```terminal
sudo docker-compose up -d
```
## Example de salida :shipit:

```shell
Building app
Sending build context to Docker daemon   5.12kB
Step 1/10 : FROM debian:bullseye-slim
 ---> dd94cb611937
Step 2/10 : RUN apt-get update
 ---> Using cache
 ---> 24b95c12047e
Step 3/10 : RUN apt-get install apache2 python3 python3-pip libmariadb-dev     libapache2-mod-wsgi-py3 python3-dev openssh-client nano -y
 ---> Using cache
 ---> 6bfbe52fb0db
Step 4/10 : ENV TZ=America/Mexico_City
 ---> Using cache
 ---> feb9bf2dd503
Step 5/10 : RUN ln -snf  /etc/l/usr/share/zoneinfo/$TZocaltime && echo $TZ > /etc/timezone
 ---> Using cache
 ---> 8d125b4f2ae8
Step 6/10 : WORKDIR /app
 ---> Using cache
 ---> ad5fec27730d
Step 7/10 : COPY ./requirements/requirements.txt /app/requirements.txt
 ---> 11838e12992e
Step 8/10 : RUN pip3 install -r /app/requirements.txt
 ---> Running in 2f80fe956533
Collecting Flask
  Downloading Flask-2.2.2-py3-none-any.whl (101 kB)
Collecting mysqlclient==2.1.0
  Downloading mysqlclient-2.1.0.tar.gz (87 kB)
Collecting Pillow==9.0.1
  Downloading Pillow-9.0.1-cp39-cp39-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (4.3 MB)
Collecting six==1.16.0
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Collecting Werkzeug>=2.2.2
  Downloading Werkzeug-2.2.2-py3-none-any.whl (232 kB)
Collecting click>=8.0
  Downloading click-8.1.3-py3-none-any.whl (96 kB)
Collecting importlib-metadata>=3.6.0
  Downloading importlib_metadata-5.2.0-py3-none-any.whl (21 kB)
Collecting itsdangerous>=2.0
  Downloading itsdangerous-2.1.2-py3-none-any.whl (15 kB)
Collecting Jinja2>=3.0
  Downloading Jinja2-3.1.2-py3-none-any.whl (133 kB)
Collecting zipp>=0.5
  Downloading zipp-3.11.0-py3-none-any.whl (6.6 kB)
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.1.1-cp39-cp39-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (25 kB)
Building wheels for collected packages: mysqlclient
  Building wheel for mysqlclient (setup.py): started
  Building wheel for mysqlclient (setup.py): finished with status 'done'
  Created wheel for mysqlclient: filename=mysqlclient-2.1.0-cp39-cp39-linux_x86_64.whl size=109350 sha256=8bcc0474f9e3824323c6738cece39852a21225bfcbc6414073be9d4ac08d8219
  Stored in directory: /root/.cache/pip/wheels/87/b7/2c/de7dba0acf2f491d1bbc8c56b7320041ef58621ea75b47bdc9
Successfully built mysqlclient
Installing collected packages: zipp, MarkupSafe, Werkzeug, Jinja2, itsdangerous, importlib-metadata, click, six, Pillow, mysqlclient, Flask
Successfully installed Flask-2.2.2 Jinja2-3.1.2 MarkupSafe-2.1.1 Pillow-9.0.1 Werkzeug-2.2.2 click-8.1.3 importlib-metadata-5.2.0 itsdangerous-2.1.2 mysqlclient-2.1.0 six-1.16.0 zipp-3.11.0
Removing intermediate container 2f80fe956533
 ---> 053cf4b13d5d
Step 9/10 : EXPOSE 80
 ---> Running in 1f2a9c48a406
Removing intermediate container 1f2a9c48a406
 ---> a8d868822ba6
Step 10/10 : CMD ["apachectl", "-D", "FOREGROUND"]
 ---> Running in 500f2946e22a
Removing intermediate container 500f2946e22a
 ---> 670e369d7791
Successfully built 670e369d7791
Successfully tagged web-server-ccpyg_app:latest
WARNING: Image for service app was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
Creating ccpygdb ... done
Creating ccpyg-webapp ... done

```

## Ver contenedores corriendo

```shell
sudo docker ps
```

## Example salida
```
CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS          PORTS                                               NAMES
########8ea5   web-server-ccpyg_app   "apachectl -D FOREGR…"   16 seconds ago   Up 15 seconds   80/tcp, 0.0.0.0:8000->8000/tcp, :::8000->8000/tcp   ccpyg-webapp
########7285   mariadb                "docker-entrypoint.s…"   16 seconds ago   Up 15 seconds   0.0.0.0:3320->3306/tcp, :::3320->3306/tcp           ccpygdb

```

# Notas adicionales

**Tener instalado docker y docker-compose**

> @Neurasystem