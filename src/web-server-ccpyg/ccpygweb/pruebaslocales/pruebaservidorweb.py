from flask import Flask,render_template
from flask_mysqldb import MySQL
#from flaskext.mysql import MySQL
import pymysql

#Funcion creacion de config app
def confApp(host,user,passwd,db):
    app = Flask(__name__)
    app.config['MYSQL_HOST'] = host
    app.config['MYSQL_USER'] = user
    app.config['MYSQL_PASSWORD'] = passwd
    app.config['MYSQL_DB'] = db

    return app

#app = confApp('db-pruebas','ccpyguser1', 'ccpyguser1','prwebapp')

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'db-pruebas'
app.config['MYSQL_USER'] = 'ccpyguser1'
app.config['MYSQL_PASSWORD'] = 'ccpyguser1'
app.config['MYSQL_DB'] = 'prwebapp'

mysql = MySQL(app)
with app.app_context():
    cur = mysql.connection.cursor()


@app.route('/')
def home():
    return render_template('home.html')

    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='83')