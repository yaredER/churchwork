# Pruebas locales

## @Author: Arturo Yared Elizondo Regino
## @Version: v1.1

## Descripcion

#### Importante

- **Debes de acceder primero al contenedor**

```
sudo docker exec -it <CONTAINER ID> /bin/bash
```

Esta carpeta solamente almacena estructuras para hacer pruebas

## Datos addicionales

[] Puerto para correr el server web #8083

> @Neurasystem