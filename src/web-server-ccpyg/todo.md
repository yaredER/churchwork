# TODO PARA CREAR APP DE REPORTES DE GRUPOS

## @Autor: Arturo Yared Elizondo Regino

## ToDo

### ToDo de Base de datos 
- Crear tablas en la base de datos pruebas
    - Logins
        - guardar el user y password
        - Cada user tendra un identificador que sera la llave secundaria
        - Ese identificador se debe de usar para extraer la informacion necesaria de los reportes
    
    - Reportes de grupos
        - Separar toda la info
        - Obtener ejemplo de reporte de grupos
    
    - Info de users
        - identificador unico de user
        - Nombres
        - Apellidos (maternos y paternos)
        - numero de telefono
        - correo electronico
    
    - Info de grupos
        - Identificador unico de grupo
        - Nombre del lider del grupo
        - Ubicacion del grupo 
        - Identificador secundario de lider (user)
    
    - Info de reporte
        - Identificador unico de reporte
        - Identificador secundario de grupo
        - Identificador secundario de user
        - Informaciond del reporte (pedir estructura del reporte que usan)

### ToDO de server flask

- Crear base de servidor
- Hacer una nueva ruta de para los reportes
- Hacer conexion a la base de datos pruebas
- Crear HTML Y CSS para los reportes

## Terminado

> @Neurasystem