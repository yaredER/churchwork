# Repositorio de trabajos - CCPYG

## Descripcion
- Este repositorio es para los trabajos dentro de 
CCPYG y creacion de apps

## Apps a crear
- Creacion de app mobil para CCPYG
- Configuracion de Redes
- Creacion de app para movimiento autonomo de camaras
- Pagina Web 

## ToDo

- Cambiar server flask fuera de docker 
- Probar server flask en todas las redes

### APP REPORTES
- Crear server base en flask para la app de reportes
- Crear contenedor con las imagenes de las bases de datos
- Crear las tablas en la base de datos pruebas 
    - Login
    - USERS_INFO
    - REPORTE_INFO
    - GRUPO_INFO

[Mas informacion para trabajar](https://gitlab.com/yaredER/churchwork/-/blob/main/src/web-server-ccpyg/todo.md)



## Hecho

- Crear Docker server (web)
    - Ubuntu
    - Mysql
- Crear server base de flask 
- Conectar mysql con flask
- Creacion de docker-compose
- Creacion de tabla en MYSQL para pruebas
    - Tabla creada USERS en la base de datos de pruebas
- Modificacion necesaria en el diagrama agregamos un modem mas un TP-LINK
## Recursos
![Diagrama de redes](https://gitlab.com/yaredER/churchwork/blob/main/recursosmd/diagrama-red-iglesia-13-11-22.jpg)


![Diagrama de server de reportes de grupo](https://gitlab.com/yaredER/churchwork/-/blob/main/recursosmd/server-reporte-grupos.png)

![Diagrama de server app web ccpyg](https://gitlab.com/yaredER/churchwork/-/blob/main/recursosmd/server-app-version1.png)


> @Neurasystem
